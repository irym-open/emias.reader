import json

log_path = 'CT_CT.log'

with open(log_path, 'r') as file_handler:
    lines = file_handler.readlines()

    results = {}

    curr_study = None
    for line in lines:
        try:
            line = line.replace("'", '"')
            line = line.replace("False", "false")
            line = line.replace("True", "true")
            info = json.loads(line)
            curr_study = info['studyIUID']

            results[info['studyIUID']] = dict(
                flag=info['aiResult']['pathologyFlag']
            )
        except json.decoder.JSONDecodeError:
            pass
        except KeyError:
            info = json.loads(line)
            str_info = info[list(info.keys())[0]]
            dict_info = json.loads(str_info)
            results[curr_study]['affection'] = dict_info['description']

    report_types = ((0.5, 'Порог разделения KT0 и KT*'), (0.1, 'Порог разделения KT0 и KT*'),
                    (1.0, 'Порог разделения KT0 и KT*'))

    print('| Исследование | Поражение | Флаг |')
    print('| ---- | ---- | ---- |')
    for entry in report_types:
        print('| ---- | ---- | ---- |')
        print(f'| {entry[1]} = {entry[0]} | ---- | ---- |')
        print('| ---- | ---- | ---- |')
        for key, result in results.items():
            if float(result['affection'].split(': ')[1]) < entry[0]:
                result['flag'] = False
            print('|', key, '|', result['affection'], '|', result['flag'], '|')


if __name__ == '__main__':
    example = "{'studyIUID': '1.2.392.200036.9116.2.5.1.37.2420762535.1586581069.426552'," \
              " 'aiResult': {'seriesIUID': '1.2.392.200036.9116.2.5.1.37.2420762535.158.11545.1025.1'," \
              " 'pathologyFlag': False, 'confidenceLevel': 100, 'modelId': 1025, 'modelVersion': '0.9.0'," \
              " 'dateTimeParams': {'downloadStartDT': '2020-08-27T10:28:19.274+0300'," \
              " 'downloadEndDT': '2020-08-27T10:29:07.486+0300'," \
              " 'processStartDT': '2020-08-27T10:29:08.665+0300', 'processEndDT': '2020-08-27T10:29:09.010+0300'}}}"

    example = example.replace("'", '"')
    example = example.replace("False", "false")
    example = example.replace("True", "true")
    example_dict = json.loads(example)
